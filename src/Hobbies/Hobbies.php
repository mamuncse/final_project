<?php
namespace App\Hobbies;
use PDO;

class Hobbies
{
    private $id='';
    private $user_id='';
    private $title ='';
    private $description ='';
    private $image='';
    private $uploaded_image='';

    public function __construct()
    {
        session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=code_of_ethics','root','');
    }
    public function setData($data='')
    {
        if (array_key_exists('id',$data))
        {
            $this->id= $data['id'];
        }
        if (array_key_exists('user_id',$data))
        {
            $this->user_id= $data['user_id'];
        }

        if (array_key_exists('title',$data))
        {
            $this->title= $data['title'];
        }
        if (array_key_exists('description',$data))
        {
            $this->description= $data['description'];
        }
        if (array_key_exists('image',$data)){
            $this->image=$data['image'];
        }
        return $this;
    }
    public function store()
    {
        try {
                $query = "INSERT INTO `hobbies`(`id`, `user_id`, `title`, `description`,`image`) VALUES (:id, :user_id, :title, :description, :uploaded_image)";
                $stmt = $this->pdo->prepare($query);
                $stmt->execute(
                    array(
                        ':id' => null,
                        ':user_id' => $this->user_id,
                        ':title' => $this->title,
                        ':description' => $this->description,
                        ':uploaded_image' => $this->uploaded_image
                    )
                );

                if ($stmt) {
                    $_SESSION['message'] = "Hobbies Successfully added";
                    header('location:hobbies_post.php');
                }
        }
        catch (PDOException $e)
        {
            echo $e->getMessage();
        }

    }
    public function read()
    {
        try
        {
            $query="SELECT * FROM `hobbies` ";
            $stmt=$this->pdo->prepare($query);
            $stmt->execute();
            $data= $stmt->fetchAll();
            return $data;
        }
        catch (PDOException $e)
        {
            echo $e->getMessage();

        }

    }
    public function show()
    {
        try
        {
            $query="SELECT * FROM `hobbies` WHERE id=".$this->id;
            $stmt= $this->pdo->prepare($query);
            $stmt->execute();
            $data=$stmt->fetchAll();
            return $data;
        }

        catch (PDOException $e)
        {
            echo $e->getMessage();

        }
    }
    public function update()
    {

        try
        {
            $query="UPDATE `hobbies` SET 
          `title` = :title,
           `description` = :description,
            `image`=:image WHERE `id`=".$this->id;
            $stmt= $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id'=>$this->id,
                    ':title' => $this->title,
                    ':description'=> $this->description,
                    ':image'=>$this->image
                )
            );
            if ($stmt)
            {
                $_SESSION['message']="Successfully updated";
                header('location:hobbies_edit.php');
            }
        }

        catch (PDOException $e)
        {
            echo $e->getMessage();

        }
    }

}