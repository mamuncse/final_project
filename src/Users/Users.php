<?php
namespace App\users;
use PDO;

class Users
{
    private $id = '';
    private $fname = '';
    private $username = '';
    private $email = '';
    private $password = '';
    private $gender = '';
    private $token = '';
    private $pdo = ' ';

    public function __construct()
    {
        //session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=code_of_ethics', 'root', '');
    }

    public function setData($data = '')
    {

        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('fname', $data)) {
            $this->fname = $data['fname'];
        }
        if (array_key_exists('username', $data)) {
            $this->username = $data['username'];
        }
        if (array_key_exists('email', $data)) {
            $this->email = $data['email'];
        }
        if (array_key_exists('password', $data)) {
            $this->password = md5($data['password']);
        }
        if (array_key_exists('gender', $data)) {
            $this->gender = $data['gender'];
        }
        if (array_key_exists('token', $data)) {
            $this->token = $data['token'];
        }

        return $this;
    }

    public function email_verify()
    {
        try {
            $query = "SELECT  * FROM `users_data` where token='$this->token'";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();
            if (!empty($data)) {
                $query = "UPDATE  `users_data` SET is_active=1 where token=:token";
                $stmt = $this->pdo->prepare($query);
                $stmt->execute(
                    array(
                        ":token" => $this->token
                    )
                );
                $_SESSION['message'] = "You'r email verified!plz login now!!";
                header("Location:index.php");
            }

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function store()
    {
        $token = sha1($this->username);
//    $hasin=md5($this->password);

        try {
            $query = "INSERT INTO `users_data` (`id`,`unique_id`,`fname`, `username`, `email`, `password`,`gender`,`token`,`is_active`) VALUES (:id, :uid, :fname, :username, :email, :password, :gender, :token, :is_active)";
            $stmt = $this->pdo->prepare($query);
            $stmt2 = $stmt->execute(
                array(
                    ":id" => null,
                    ":uid" => uniqid(),
                    ":fname" => $this->fname,
                    ":username" => $this->username,
                    ":email" => $this->email,
                    ":password" => $this->password,
                    ":gender" => $this->gender,
                    ":token" => $token,
                    ":is_active" => 0
                ));
            $link = "Click here for email verify.http://mamunbd.com/verify.php?token=$token";

            $subject = "email verification";
            mail($this->email, $subject, $link);

            if ($stmt2) {

                $_SESSION['message'] = "<span style='color:green;font-size:20px;'>Data inserted successfully and you'r email verify!!</span>";
                header("Location:registration.php");
            }
        } catch (PDOExecption $e) {
            echo "ERROR:" . $e->getMessage();
        }
    }

    public function login()
    {
        try {
            $query = "SELECT * FROM `users_data` WHERE username='$this->username' AND password='$this->password'";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();
            if ($data['is_active'] == '0') {
                $_SESSION['fail'] = "You'r Email not verified yet";
                header("Location:index.php");
            } else {
                $_SESSION['user_info'] = $data;
                if (!empty($_SESSION['user_info'])) {
                    header("Location:dashboard.php");
                } else {
                    $_SESSION['fail'] = "Username or Password not match";
                    header("location:index.php");
                }
            }

        } catch (PDOException $e) {
            echo "ERROR:" . $e->getMessage();
        }
    }

    public function validity()
    {
        try {
            $query = "SELECT  * FROM `users_data` where email='$this->email' OR username='$this->username'";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();
            return $data;

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }}