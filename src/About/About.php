<?php
namespace App\About;
use PDO;
class About
{
    private $id = '';
    private $user_id = '';
    private $title = '';
    private $phone = '';
    private $bio = '';
    private $pdo = ' ';

    public function __construct()
    {
        session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=code_of_ethics', 'root', '');
    }

    public function setData($data = '')
    {
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('user_id', $data)) {
            $this->user_id = $data['user_id'];
        }
        if (array_key_exists('title', $data)) {
            $this->title = $data['title'];
        }
        if (array_key_exists('phone', $data)) {
            $this->phone = $data['phone'];
        }
        if (array_key_exists('bio', $data)) {
            $this->bio = $data['bio'];
        }

        return $this;

    }

    public function store()
    {

        try {
            $query = "INSERT INTO `about` (`id`,`user_id`, `title`, `phone`, `bio`) VALUES (:id, :user_id, :title, :phone, :bio)";
            $stmt = $this->pdo->prepare($query);
            $stmt2 = $stmt->execute(
                array(
                    ":id" => null,
                    ":user_id" =>$this->user_id,
                    ":title" => $this->title,
                    ":phone" => $this->phone,
                    ":bio" => $this->bio
                ));

            if ($stmt2) {

                $_SESSION['message'] = "<span style='color:green;font-size:20px;'>Data Saved successfully</span>";
                header("Location:about_list.php");
            }
        } catch (PDOExecption $e) {
            echo "ERROR:" . $e->getMessage();
        }
    }
    public function read()
    {
        try {
            $query = "SELECT * FROM `about`";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchAll();
            return $data;
        } catch (PDOExecption $e) {
            echo "ERROR:" . $e->getMessage();
        }
    }
    public function update(){
        //$user_id= "'".$this->user_id ."'";
        try{
            $query="UPDATE `about`
            SET
            `title`=:title,
            `phone`=:phone,
            `bio`=:bio
            WHERE `id`= :id";
            $stmt=$this->pdo->prepare($query);
                $stmt->execute(
                    array(
                        ":id"=>$this->id,
                        ":title" => $this->title,
                        ":phone" => $this->phone,
                        ":bio" => $this->bio
                    )
                );
                if ($stmt){
                    $_SESSION['message']="Updated successfuly!!";
                    header("Location:about_edit.php");
                }
        }catch (PDOException $e){
            echo "ERROR:".$e->getMessage();
        }
    }
    public function show()
    {
       // $userid= "'".$this->user_id ."'";
        try {
            $query = "SELECT * FROM `about` where id=".$this->id;
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchAll();
            return $data;
        } catch (PDOExecption $e) {
            echo "ERROR:" . $e->getMessage();
        }
    }
}