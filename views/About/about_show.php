<?php
include_once("../../vendor/autoload.php");
use App\About\About;
$obj = new About();
$obj->setData($_GET);
$value=$obj->show();
if (!empty($_SESSION['user_info'])) { ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>cvbank/about</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
          type="text/css">
    <link href="../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="../assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="../assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="../assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="../assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="../assets/js/core/app.js"></script>
    <script type="text/javascript" src="../assets/js/pages/user_pages_list.js"></script>
    <!-- /theme JS files -->
    <!--Fancy Button-->
    <script src="js/fancy-button/fancy-button.js" type="text/javascript"></script>
    <script src="js/setup.js" type="text/javascript"></script>
    <script src="js/tiny-mce/jquery.tinymce.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            setupTinyMCE();
            setDatePicker('date-picker');
            $('input[type="checkbox"]').fancybutton();
            $('input[type="radio"]').fancybutton();
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            setupLeftMenu();
            setSidebarHeight();
        });
    </script>
</head>

<body>

<!-- Main navbar -->
<?php include("../inc/header.php"); ?>
<!-- /main navbar -->


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <?php include("../inc/main_sidebar.php"); ?>
        <!-- /main sidebar -->


        <!-- Main content -->
        <!-- Main content -->
        <?php include ("../inc/page_header.php");?>
        <!-- /page header -->

        <h2>
            <?php
            echo '<h2 style="color:#006400; font-size:20px; text-align:center;">';
            if (isset($_SESSION['message'])) {
                echo $_SESSION['message'];
                unset($_SESSION['message']);
            }
            echo '</h2>';
            echo '<h2 style="color:red; font-size:20px; text-align:center;">';
            if (isset($_SESSION['fail'])) {
                echo $_SESSION['fail'];
                unset($_SESSION['fail']);
            }
            echo '<h2>';
            ?>
        </h2>
                <table class="form">

                    <tr>
                        <td>
                            <label>Title</label>
                        </td>
                        <td>
                            <?php echo $value['title'];?>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label>phone</label>
                        </td>
                        <td>
                            <?php echo $value['phone'];?>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top; padding-top: 9px;">
                            <label>Bio</label>
                        </td>
                        <td>
                            <?php echo $value['bio'];?>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="submit" Value="Update"/>
                        </td>
                    </tr>
                </table>




        <!-- Footer -->
        <?php include("../inc/footer.php"); ?>
        <!-- /footer -->
        <?php } ?>
