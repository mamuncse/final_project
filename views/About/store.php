<?php
include_once("../../vendor/autoload.php");
use App\About\About;
$obj = new About();
if ($_SERVER['REQUEST_METHOD']=="POST") {
    if (!empty($_POST['title']) OR !empty($_POST['phone']) OR !empty($_POST['bio'])) {
        if(preg_match("/([0-9])/",$_POST['phone'])) {
            $obj->setData($_POST)->store();
        }else{
            $_SESSION['fail']="Invalid phone no.Plz input 0 to 9 ";
            header("Location:about.php");
        }
    }else{
        $_SESSION['fail']="Field must not be empty !!";
        header("Location:about.php");
    }
}else{
    header("Location:login.php");
}