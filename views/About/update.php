<?php
include_once("../../vendor/autoload.php");
use App\About\About;
$obj = new About();
if ($_SERVER['REQUEST_METHOD']=="POST") {
    if (!empty($_POST['title']) OR !empty($_POST['phone']) OR !empty($_POST['bio'])) {
        $obj->setData($_POST)->update();
    }else{
        $_SESSION['fail']="Field must not be empty !!";
        header("Location:about.php");
    }
}