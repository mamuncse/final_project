<div class="sidebar sidebar-main">
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user">
            <div class="category-content">
                <div class="media">
                    <a href="#" class="media-left"><img src="../images/mamun.jpg" class="img-circle img-sm" alt=""></a>
                    <div class="media-body">
                        <span style='color:yellowgreen; font-size:17px;' class="media-heading text-semibold"><?php echo $_SESSION['user_info']['username'];?></span>
                    </div>
                </div>
            </div>
        </div>
        <!-- /user menu -->


        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">

                    <li class="active"><a href="dashboard.php"><i class="icon-home4"></i> <span>Dashboard</span></a></li>






                    <!-- Page kits -->
                    <li class="navigation-header"><span>Page kits</span> <i class="icon-menu" title="Page kits"></i></li>
                    <li>
                        <a href="#"><i class="icon-people"></i> <span>About</span></a>
                        <ul>
                            <li><a href="../About/about.php">About_Post</a></li>
                            <li><a href="../About/about_edit.php">About_update</a></li>
                            <li><a href="../About/about_list.php">About_List</a></li>
                            <li><a href="../About/about_delete.php">About_Delete</a></li>

                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="icon-people"></i> <span>Setting</span></a>
                        <ul>
                            <li><a href="setting_post.php">Setting_Post</a></li>
                            <li><a href="setting_edit.php">Setting_update</a></li>
                            <li><a href="setting_list.php">Setting_List</a></li>
                            <li><a href="setting_delete.php">Setting_Delete</a></li>

                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="icon-people"></i> <span>Hobbies</span></a>
                        <ul>
                            <li><a href="../Hobbies/hobbies_post.php">Hobbies_Post</a></li>
                            <li><a href="../Hobbies/hobbies_edit.php">Hobbies_update</a></li>
                            <li><a href="../Hobbies/hobbies_list.php">Hobbies_List</a></li>
                            <li><a href="../Hobbies/hobbies_delete.php">hobbies_Delete</a></li>

                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="icon-people"></i> <span>Eduction</span></a>
                        <ul>
                            <li><a href="Eduction_post.php">Education_Post</a></li>
                            <li><a href="Eduction_edit.php">Education_update</a></li>
                            <li><a href="Eduction_list.php">Education_List</a></li>
                            <li><a href="Eduction_delete.php">Education_Delete</a></li>

                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="icon-people"></i> <span>Experience</span></a>
                        <ul>
                            <li><a href="Experience_post.php">Experience_Post</a></li>
                            <li><a href="Experience_edit.php">Experience_update</a></li>
                            <li><a href="Experience_list.php">Experience_List</a></li>
                            <li><a href="Experience_delete.php">Experience_Delete</a></li>

                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="icon-people"></i> <span>Awards</span></a>
                        <ul>
                            <li><a href="Awards_post.php">Awards_Post</a></li>
                            <li><a href="Awards_edit.php">Awards_update</a></li>
                            <li><a href="Awards_list.php">Awards_List</a></li>
                            <li><a href="Awards_delete.php">Awards_Delete</a></li>

                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="icon-people"></i> <span>Services</span></a>
                        <ul>
                            <li><a href="Services_post.php">Services_Post</a></li>
                            <li><a href="Services_edit.php">Services_update</a></li>
                            <li><a href="Services_list.php">Services_List</a></li>
                            <li><a href="Services_delete.php">Services_Delete</a></li>

                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="icon-people"></i> <span>skill</span></a>
                        <ul>
                            <li><a href="skill_post.php">skill_Post</a></li>
                            <li><a href="skill_edit.php">skill_update</a></li>
                            <li><a href="skill_list.php">skill_List</a></li>
                            <li><a href="skill_delete.php">skill_Delete</a></li>

                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="icon-people"></i> <span>portfolios</span></a>
                        <ul>
                            <li><a href="portfolios_post.php">skill_Post</a></li>
                            <li><a href="portfolios_edit.php">skill_update</a></li>
                            <li><a href="portfolios_list.php">skill_List</a></li>
                            <li><a href="portfolios_delete.php">skill_Delete</a></li>

                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="icon-search4"></i> <span>Search</span></a>
                        <ul>
                            <li><a href="search_basic.php"> search results</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="icon-warning"></i> <span>Error pages</span></a>
                        <ul>
                            <li><a href="error_404.html">Error 404</a></li>
                        </ul>
                    </li>
                    <!-- /page kits -->

                </ul>
            </div>
        </div>
        <!-- /main navigation -->

    </div>
</div>